/**
 * Created by samuel on 10/07/2020.
 */
import axios from 'axios'
import moment from 'moment'
import Vue from 'vue'

axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL + ''

const network = axios.create({
  headers: {
    common: {
      'Accept': 'application/json',
      'Cache-Control': 'no-cache,no-store'
    }
  }
})

network.interceptors.response.use(
  response => {
    // return response.data
    return convertDateToMoment(response.data)
  },

  error => {
    if (error.response) {
        Vue.snackbar('error', 'Une erreur est survenue')
    }

    throw error
  }
)

function convertDateToMoment (obj) {
  if (obj instanceof Array) {
    return obj.map(item => convertDateToMoment(item))
  } else if (typeof obj === 'object') {
    for (let key in obj) {
      obj[key] = convertDateToMoment(obj[key])

      if (obj[key] && (key.indexOf('date') !== -1 || key.endsWith('At'))) {
        obj[key] = moment(obj[key])
      }
    }

    return obj
  }

  return obj
}
export default network
