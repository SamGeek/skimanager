import network from '../axio'
import error from '../helpers/errorNetwork'

export default {
  create (data) {
    return network.post('/Facture', data).catch((err) => {
      error.handleExeption(err)
      return err
    })
  },

  delete (id) {
    return network.delete('/Facture/' + id).catch((err) => {
      error.handleExeption(err)
      return err
    })
  },

  update (data) {
    return network.put('/Facture/' + data.id, data).catch((err) => {
      error.handleExeption(err)
      return err
    })
  },

  get (id) {
    if (id) {
      return network.get('/organiser/fetchStays?userID='+ id).catch((err) => {
        error.handleExeption(err)
        return err
      })
    } else {
      return network.get('/stay').catch((err) => {
        error.handleExeption(err)
        return err
      })
    }
  },

  getAccepted () {
    return network.get(`/Facture?where="status": "ACCEPT"`).catch((err) => {
      error.handleExeption(err)
      return err
    })
  },

  getPopulatedData (programId, id) {
    if (id) {
      return network.get('/Facture/' + id + '?populate=commande').catch((err) => {
        error.handleExeption(err)
        return err
      })
    } else {
      return network.get(`/Facture?where=${JSON.stringify({
        donneurdordre: programId,
        or: [
          { status: 'SEND' },
          { status: 'ACCEPT' }
        ]
      })}&sort=createdAt DESC`).catch((err) => {
        error.handleExeption(err)
        return err
      })
    }
  },
  getBillByProvider (programId, id) {
    return network.get(`/Facture?where=` + JSON.stringify({
      fournisseur: id,
      donneurdordre: programId
    }) + '&sort=createdAt DESC').catch((err) => {
      error.handleExeption(err)
      return err
    })
  }
}
