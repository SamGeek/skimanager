import network from '../axio'
import error from './errorNetwork'

export default function (route) {
  return {
    create (data) {
      return network.post(route, data).catch((err) => {
        error.handleExeption(err)
        throw err
      })
    },

    update (data) {
      return network.put(route + '/' + data.id, data).catch((err) => {
        error.handleExeption(err)
        throw err
      })
    },

    get (id) {
      if (id) {
        return network.get(route + '/' + id, {
          params: {
            sort: 'createdAt DESC'
          }
        }).catch((err) => {
          error.handleExeption(err)
          throw err
        })
      } else {
        return network.get(route).catch((err) => {
          error.handleExeption(err)
          throw err
        })
      }
    },

    delete (id) {
      return network.delete(route + '/' + id).catch((err) => {
        error.handleExeption(err)
        throw err
      })
    }
  }
}
