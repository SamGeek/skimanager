
export const fieldRequire = [
  v => !!v || 'Ce champ est requis'
]

export const selectNatJuRules = [
  v => !!v || 'Veuillez choisir une nature juridique'
]

export const nineaRule = [
  v => !!v || 'Ce champ est requis',
  v => (v && /\d{7}/.test(v)) || 'Le ninea doit contenir 7 chiffres'
]

export const numberNotEmpty = [
  (v) => !!v || /^[$%]?(((-?(?!0)(([0-9]{1,3})(,\d{3})*|\d+)|0)?(\.\d+)?)|[(](((?!0)(([1-9]{1,3})(,\d{3})*|\d+)|0)?(\.\d+)?)[)])$/.test(v) || 'Entrez des chiffres uniquement'
]

export const numberRequire = [
  (v) => /^[$%]?(((-?(?!0)(([0-9]{1,3})(,\d{3})*|\d+)|0)?(\.\d+)?)|[(](((?!0)(([1-9]{1,3})(,\d{3})*|\d+)|0)?(\.\d+)?)[)])$/.test(v) || 'Entrez des chiffres uniquement'
]

export const numberRequireNonEmpty = [
  (v) => !!v || /^[$%]?(((-?(?!0)(([0-9]{1,3})(,\d{3})*|\d+)|0)?(\.\d+)?)|[(](((?!0)(([1-9]{1,3})(,\d{3})*|\d+)|0)?(\.\d+)?)[)])$/.test(v) || 'Entrez des chiffres uniquement'
]

export const numberEmptyRule = [
  (v) => !v || /^[$%]?(((-?(?!0)(([0-9]{1,3})(,\d{3})*|\d+)|0)?(\.\d+)?)|[(](((?!0)(([1-9]{1,3})(,\d{3})*|\d+)|0)?(\.\d+)?)[)])$/.test(v) || 'Entrez des chiffres uniquement'
]

export const numberFormatedWithSpacesRule = [
  v => !v || /^[$%]?(((-?(?!0)(([0-9]{1,3})( \d{3})*|\d+)|0)?(\.\d+)?)|[(](((?!0)(([1-9]{1,3})( \d{3})*|\d+)|0)?(\.\d+)?)[)])$/ || 'Entrez un nombre valide'
]

export const numberRequiredRule = [
  v => !!v || 'Ce champ est requis',
  v => /^[$%]?(((-?(?!0)(([0-9]{1,3})(,\d{3})*|\d+)|0)?(\.\d+)?)|[(](((?!0)(([1-9]{1,3})(,\d{3})*|\d+)|0)?(\.\d+)?)[)])$/.test(v) || 'Entrez des chiffres uniquement'
]

export const emailRules = [
  (v) => !v || /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || 'Entrez un email valide'
]

export const phoneEmptyNumberRules = [
  (v) => !v || new RegExp('^[]?[]?[0-9]{3}[]?[s.]?[0-9]{3}[s.]?[0-9]{2,4}$', 'im').test(v) || 'Entrez un numéro de téléphone valide (Minimum 8 Chiffres)'
]
export const phoneNumberRules = [
  (v) => new RegExp('^[]?[]?[0-9]{3}[]?[s.]?[0-9]{3}[s.]?[0-9]{2,4}$', 'im').test(v) || 'Entrez un numéro de téléphone valide (Minimum 8 Chiffres)'
]
export const alphaPercent = [
  (v) => new RegExp('^[0-9]{1,2}$', 'i').test(v) || 'Entrez un pourcentage valide ( < 100 )'
]

export const alphaFloatPercent = [
  (v) => new RegExp('^\\d+(.\\d+)?$', 'i').test(v) || 'Entrez un pourcentage valide ( < 100 )'
]
export const alphaFloatWithPercent = [
  (v) => new RegExp('^[0-9\\.]+$', 'i').test(v) || 'Entrez un pourcentage valide ( < 100 )'
]

export const validTermAmount = [
  (v) => new RegExp('^[0-9]{1,15}$', 'i').test(v) || 'Entrez un montant valide ( < 1.000.000.000.000.000 XOF )'
]

export const emailRequiredRules = [
  v => !!v || 'Ce champ est requis',
  v => (v && /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v)) || 'Entrez un email valide'
]

export default {
  fieldRequire,
  selectNatJuRules,
  nineaRule,
  numberNotEmpty,
  numberRequire,
  numberRequireNonEmpty,
  numberEmptyRule,
  numberRequiredRule,
  emailRules,
  phoneNumberRules,
  phoneEmptyNumberRules,
  alphaPercent,
  alphaFloatPercent,
  validTermAmount,
  emailRequiredRules
}
