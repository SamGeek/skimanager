import countries from 'countries-list'

export const natureJuridique = [
  { id: 1, text: 'Société Anonyme - SA' },
  { id: 2, text: 'Société en Participation' },
  { id: 3, text: 'Société de Fait' },
  { id: 4, text: 'Société à Responsabilité Limitée - SARL' },
  { id: 5, text: 'Société en Commandite Simple - SCS' },
  { id: 6, text: 'Société en Nom Collectif - SNC' },
  { id: 7, text: 'Collectivité Locale' },
  { id: 8, text: 'Groupement d\'Intérêt Economique - GIE' },
  { id: 9, text: 'Société par Actions Simplifiée - SAS' }
  // { id : 9, text: 'Société Anonyme Unipersonnelle' },
  // { id : 9, text: 'Société Unipersonnelle à Responsabilité Limitée' },
]

export const natureJuridiqueProvider = [
  { id: 1, text: 'Société Anonyme - SA' },
  { id: 2, text: 'Société en Participation' },
  { id: 3, text: 'Société de Fait' },
  { id: 4, text: 'Société à Responsabilité Limitée - SARL' },
  { id: 5, text: 'Société en Commandite Simple - SCS' },
  { id: 6, text: 'Société en Nom Collectif - SNC' },
  { id: 7, text: 'Collectivité Locale' },
  { id: 8, text: 'Groupement d\'Intérêt Economique - GIE' },
  { id: 9, text: 'Société par Actions Simplifiée - SAS' },
  { id: 10, text: 'Société Anonyme Unipersonnelle' },
  { id: 11, text: 'Société Unipersonnelle à Responsabilité Limitée' },
  { id: 12, text: 'Autre' }
]

export const regions = [
  'Dakar',
  'Diourbel',
  'Fatick'
]

export const departements = [
  'Dakar',
  'Guédiawaye',
  'Pikine',
  'Rufisque'
]

export const arrondissements = [
  'Almadies',
  'Dakar Plateau',
  'Grand Dakar'
]

export const villes = [
  'Dakar',
  'Pikine',
  'Touba',
  'Guédiawaye',
  'Thiès'
]

export const representantTypes = [
  'Carte Nationale d\'Indentité',
  'Passeport'
]

export const functionsDO = [
  'Responsable IT/Juridiction RF',
  'Directeur Financier',
  'Chef Comptable',
  'Responsable Tresorerie',
  'Compatble Fournisseur',
  'Responsable Achat',
  'Autres Utilisateurs interne'
]

export const functionsFrs = [
  'Directeur General / Responsable Juridiction',
  'Directeur Financier',
  'Chef Comptable Client',
  'Autres utilisateurs'
]

export const functionsFrsExterne = [
  'Commissaires Aux Comptes',
  'Comptable Externe'
]

export const functionsBanque = [
  'Responsable IT / Responsable Juridiction',
  'Chargé de Compte',
  'N+1 Chargé de Compte'
]

export const typeBank = [
  'Principal',
  'Secondaire'
]

export const bankDetails = [
  { id: 1, text: 'Banque de Détails au Sénégal' },
  { id: 2, text: 'Banque de Détails et Services Financiers Internationaux' },
  { id: 3, text: 'Banque de Grande Clientèle et Solutions Inverstisseurs' },
  { id: 4, text: 'Autre' }
]

export function pays () {
  let allCountries = []

  for (let p in countries.countries) {
    allCountries.push({
      code: p,
      name: countries.countries[p].name,
      nameWithFlag: countries.getEmojiFlag(p) + ' ' + countries.countries[p].name,
      flag: countries.getEmojiFlag(p) + ' ' + countries.countries[p].phone,
      phone: countries.countries[p].phone
    })
  }

  return allCountries
}

export const COUNTRIES = pays()

export function getCountryByCode (code) {
  return this.pays().find(function (pays) {
    return pays.code === code
  })
}

// TODO : Why the hell this is wrapped in a function ?
export function SENEGAL () {
  return {
    code: 'SN',
    name: countries.getEmojiFlag('SN') + ' ' + countries.countries['SN'].name,
    flag: countries.getEmojiFlag('SN') + ' ' + countries.countries['SN'].phone
  }
}

export default {
  natureJuridique,
  natureJuridiqueProvider,
  regions,
  departements,
  arrondissements,
  villes,
  representantTypes,
  functionsDO,
  functionsFrs,
  functionsFrsExterne,
  functionsBanque,
  typeBank,
  bankDetails,

  pays,
  getPayByCode: getCountryByCode,
  SENEGAL,
  COUNTRIES
}
