import Vue from "vue";
import VueRouter from "vue-router";
import Sejours from "@/components/Stays.vue";
import DetailSejour from "@/components/StayDetail.vue";
import AjouterSejour from "@/components/AddStay.vue";
import Utilisateurs from "@/components/UsersManagment.vue";
import Pricing from "@/components/Pricing.vue";
import Chalet from "@/components/Chalet.vue";
import Skipass from "@/components/Skipass.vue";
import Bill from "@/components/ShowBill.vue";
import DetailsFacture from "@/components/DetailsFacture.vue";
import Bills from "@/components/Bills.vue";
import UserStayContent from "@/components/ShowUserStayContent.vue";
import ShowBillSaved from "@/components/ShowBillSaved.vue";
import PaymentDetail from "@/components/PaymentStayDetail.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Sejours
  },
  {
    path: "/detail_sejour",
    name: "detailSejour",
    component: DetailSejour,
    props: true
  },
  {
    path: "/chalet",
    name: "chalet",
    component: Chalet,
    props: true
  },
  {
    path: "/skipass",
    name: "skipass",
    component: Skipass,
    props: true
  },
  {
    path: "/ajouter_sejour",
    name: "ajouterSejour",
    component: AjouterSejour
  },
  {
    path: "/utilisateurs",
    name: "utilisateurs",
    component: Utilisateurs
  },
  {
    path: "/pricing",
    name: "pricing",
    component: Pricing
  },
  {
    path: "/bill",
    name: "bill",
    component: Bill,
    props: true
  },
  {
    path: "/bill_saved",
    name: "bill_saved",
    component: ShowBillSaved,
    props: true
  },
  {
    path: "/details_participant",
    name: "details_participant",
    component: UserStayContent,
    props: true
  },
  {
    path: "/details_paiement",
    name: "details_paiement",
    component: PaymentDetail,
    props: true
  },
  {
    path: "/details_facture",
    name: "details_facture",
    component: DetailsFacture,
    props: true
  },
  {
    path: "/factures",
    name: "factures",
    component: Bills,
    props: true
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
